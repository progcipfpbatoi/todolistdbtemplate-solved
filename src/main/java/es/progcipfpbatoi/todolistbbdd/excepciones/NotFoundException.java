package es.progcipfpbatoi.todolistbbdd.excepciones;

public class NotFoundException extends Exception {

    public NotFoundException(String message) {
        super(message);
    }
}
