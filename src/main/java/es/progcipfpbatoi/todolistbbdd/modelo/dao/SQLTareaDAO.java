package es.progcipfpbatoi.todolistbbdd.modelo.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.progcipfpbatoi.todolistbbdd.excepciones.DatabaseErrorException;
import es.progcipfpbatoi.todolistbbdd.excepciones.NotFoundException;
import es.progcipfpbatoi.todolistbbdd.modelo.dao.interfaces.TareaDao;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Prioridad;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Tarea;
import es.progcipfpbatoi.todolistbbdd.modelo.services.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

@Service
public class SQLTareaDAO implements TareaDao {

    private static final String DATABASE_TABLE = "tareas";

    @Autowired
    private MySQLConnection mySQLConnection;

    @Override
    public ArrayList<Tarea> findAll() {
    	Connection connection = mySQLConnection.getConnection();
    	String sql = "SELECT * FROM tareas";
        ArrayList<Tarea> tareas = new ArrayList<>();
        
    	try (
			Statement ps = connection.createStatement();
			ResultSet rs = ps.executeQuery(sql)){
            
            while (rs.next()) {
                tareas.add(mapToTarea(rs));
            }
            return tareas;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    @Override
    public ArrayList<Tarea> findAllWithParams(Boolean isRealizada, LocalDate vencimiento, String usuario) {
    	
    	ArrayList<Tarea> tareas = new ArrayList<>();
        String sql = String.format("SELECT * FROM %s WHERE ", DATABASE_TABLE);
        StringBuilder where = new StringBuilder();
        if (isRealizada != null) {
            where.append(String.format("realizada = %d ", (isRealizada) ? 1 : 0));
        }
        if (vencimiento != null) {
            where.append(String.format("%s date(vencimiento) LIKE '%s%%' ", (where.length() > 0) ? "AND" : "", vencimiento));
        }
        if (usuario != null) {
            where.append(String.format("%s usuario LIKE '%%%s%%' ", (where.length() > 0) ? "AND" : "", usuario));
        }
        
    	Connection connection = mySQLConnection.getConnection();
    	
    	try (
    		Statement ps = connection.createStatement();
    		ResultSet rs = ps.executeQuery(sql + where);
    		){            
            
            while (rs.next()) {
                tareas.add(mapToTarea(rs));
            }
            return tareas;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }
    	
    @Override
    public ArrayList<Tarea> findAllWithParams(String usuario) {
    	Connection connection = mySQLConnection.getConnection();
    	ArrayList<Tarea> tareas = new ArrayList<>();
    	
    	String sql = String.format("SELECT * FROM %s WHERE usuario = ? ", DATABASE_TABLE);
    	
        try (PreparedStatement ps = connection.prepareStatement(sql)){
        	
        	ps.setString(1, usuario);
        	
            try (ResultSet rs = ps.executeQuery()) { 
	            while (rs.next()) {
	                tareas.add(mapToTarea(rs));
	            }
            }
            return tareas;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }
    
    @Override
    public Tarea getById(int id) throws NotFoundException {
    	 Tarea tarea = findById(id);
         if (tarea == null) {
             throw new NotFoundException("La tarea con código " + id + " no existe");
         }
         return tarea;
    }

    
    @Override
    public Tarea findById(int id) {
    	String sql = String.format("SELECT * FROM %s WHERE codigo = ? ", DATABASE_TABLE);
    	
    	Connection connection = mySQLConnection.getConnection();
    	
        try (PreparedStatement ps = connection.prepareStatement(sql)){
        	
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return mapToTarea(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    
    @Override
    public void add(Tarea tarea) {
    	String sql = String.format("INSERT INTO %s (codigo, usuario, descripcion, fechaCreacion, vencimiento, prioridad, realizada) " +
                "VALUES (?,?,?,?,?,?,?) ", DATABASE_TABLE);
        try (
                Connection connection = mySQLConnection.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
        ){
            ps.setNull(1, Types.INTEGER);
            ps.setString(2, tarea.getUsuario());
            ps.setString(3, tarea.getDescripcion());
            ps.setTimestamp(4, Timestamp.valueOf(tarea.getCreadoEn()));
            ps.setTimestamp(5, Timestamp.valueOf(tarea.getVencimiento()));
            ps.setString(6, tarea.getPrioridad().toString().toUpperCase());
            ps.setBoolean(7, tarea.isRealizado());
            
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                int autoIncremental = rs.getInt(1);
                tarea.setCodigo(autoIncremental);
            }

        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }
    
    
    @Override
    public void update(Tarea tarea) throws NotFoundException {
    	String sql = String.format("UPDATE %s SET usuario = ?, " +
                "descripcion = ?, fechaCreacion = ?, vencimiento = ?, prioridad = ?," +
                " realizada = ? WHERE codigo = ?", DATABASE_TABLE);
        try (
                Connection connection = mySQLConnection.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
        ) {
           ps.setString(1, tarea.getUsuario());
           ps.setString(2, tarea.getDescripcion());
           ps.setTimestamp(3, Timestamp.valueOf(tarea.getCreadoEn()));
           ps.setTimestamp(4, Timestamp.valueOf(tarea.getVencimiento()));
           ps.setString(5, tarea.getPrioridad().toString().toUpperCase());
           ps.setBoolean(6, tarea.isRealizado());
           ps.setInt(7, tarea.getCodigo());
           ps.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    @Override
    public void delete(Tarea tarea) throws NotFoundException{
    	String sql = String.format("DELETE FROM %s WHERE codigo = ? ", DATABASE_TABLE);
    	
    	Connection connection = mySQLConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setInt(1, tarea.getCodigo());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Extrae de @resultset todos los datos asociados a una tarea para crearla y devolverla
     *
     * @param resultset
     * @return Tarea
     */
    private Tarea mapToTarea(ResultSet resultset) throws SQLException {
    	int cod = resultset.getInt("codigo");
		String usuario = resultset.getString("usuario");
		String descripcion = resultset.getString("descripcion");
		LocalDateTime creadoEn = resultset.getTimestamp("fechaCreacion").toLocalDateTime();
		Prioridad priority = Prioridad.fromText(resultset.getString("prioridad"));
		LocalDateTime vencimiento = resultset.getTimestamp("vencimiento").toLocalDateTime();
		boolean realizada = resultset.getBoolean("realizada");
		
		return new Tarea(cod, usuario, descripcion, creadoEn, priority, vencimiento.toLocalDate(), 
				vencimiento.toLocalTime(), realizada);
    }

    
}
